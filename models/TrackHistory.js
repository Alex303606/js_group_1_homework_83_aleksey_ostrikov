const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TrackHistorySchema = new Schema({
	user: {
		type: Schema.Types.ObjectId,
		ref: 'Users',
		required: true
	},
	track: {
		type: Schema.Types.ObjectId,
		ref: 'Tracks',
		required: true
	},
	dateTime: {
		type: String,
		required: true
	}
	
});

const TrackHistory = mongoose.model('TrackHistory', TrackHistorySchema);

module.exports = TrackHistory;
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;

const Schema = mongoose.Schema;

const UsersSchema = new Schema({
	username: {
		type: String,
		required: true,
		unique: true
	},
	password: {
		type: String,
		required: true
	},
	token: {
		type: String,
		unique: true
	}
});

UsersSchema.pre('save', async function (next) {
	if (!this.isModified('password')) return next();
	const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
	const hash = await bcrypt.hash(this.password, salt);
	
	this.password = hash;
	
	next();
});

UsersSchema.set('toJSON', {
	transform: (doc, ret, options) => {
		delete ret.password;
		return ret;
	}
});


const Users = mongoose.model('Users', UsersSchema);

module.exports = Users;
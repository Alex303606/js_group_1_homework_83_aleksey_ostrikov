const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ArtistsSchema = new Schema({
	name: {
		type: String,
		required: true,
		unique: true
	},
	description: String,
	image: String
});

const Artists = mongoose.model('Artists', ArtistsSchema);

module.exports = Artists;
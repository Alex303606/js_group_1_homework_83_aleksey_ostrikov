const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const Albums = require('../models/Albums');

const config = require('../config');

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
	
	router.post('/', upload.single('image'), (req, res) => {
		const albumData = req.body;
		if (req.file) {
			albumData.image = req.file.filename;
		} else {
			albumData.image = null;
		}
		
		const album = new Albums(albumData);
		
		album.save()
		.then(result => res.send(result))
		.catch(error => res.status(400).send(error));
	});
	
	router.get('/', (req, res) => {
		const id = req.query.artist;
		if (id) {
			Albums.find({artist: id})
			.then(result => {
				if (result) res.send(result);
				else res.sendStatus(404);
			})
			.catch(() => res.sendStatus(500));
		} else {
			Albums.find()
			.then(result => {
				if (result) res.send(result);
				else res.sendStatus(404);
			})
			.catch(() => res.sendStatus(500));
		}
	});
	
	router.get('/:id', (req, res) => {
		const id = req.params.id;
		Albums.findOne({_id: id}).populate('artist')
		.then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	return router;
};

module.exports = createRouter;
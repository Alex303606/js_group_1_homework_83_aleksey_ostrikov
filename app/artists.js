const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const Artists = require('../models/Artists');

const config = require('../config');

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
	
	router.post('/', upload.single('image'), (req, res) => {
		const artistData = req.body;
		if (req.file) {
			artistData.image = req.file.filename;
		} else {
			artistData.image = null;
		}
		
		const artist = new Artists(artistData);
		
		artist.save()
			.then(result => res.send(result))
			.catch(error => res.status(400).send(error));
	});
	
	router.get('/', (req, res) => {
		Artists.find()
		.then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	return router;
};

module.exports = createRouter;
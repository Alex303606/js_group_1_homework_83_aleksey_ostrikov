const express = require('express');
const bcrypt = require('bcrypt');
const Users = require('../models/Users');
const nanoid = require('nanoid');
const multer = require('multer');

const router = express.Router();

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const createRouter = () => {
	
	router.post('/', upload.fields([
		{username: '', maxCount: 1},
		{password: '', maxCount: 1}
	]), (req, res) => {
		const user = new Users(req.body);
		user.save()
		.then(user => res.send(user))
		.catch(error => res.status(400).send(error))
	});
	
	router.post('/sessions', upload.fields([
		{username: '', maxCount: 1},
		{password: '', maxCount: 1}
	]), async (req, res) => {
		const user = await Users.findOne({username: req.body.username});
		
		if (!user) {
			return res.status(400).send({error: 'Username not found'});
		}
		
		// req.body.password
		const isMatch = await bcrypt.compare(req.body.password, user.password);
		
		if (!isMatch) {
			return res.status(400).send({error: 'Password is wrong!'});
		}
		user.token = nanoid();
		await user.save();
		const token = user.token;
		return res.send({message: 'User and password correct!', token});
	});
	
	return router;
};

module.exports = createRouter;
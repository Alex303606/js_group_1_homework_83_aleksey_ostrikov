const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const auth = require('../middleware/auth');

const router = express.Router();

const createRouter = () => {
	router.post('/', auth, (req, res) => {
		const track = {};
		track.track	= req.body.track;
		track.dateTime = new Date();
		track.user = req.user._id;
		const history = new TrackHistory(track);
		
		history.save()
		.then(result => res.send(result))
		.catch(error => res.status(400).send(error));
	});
	
	return router;
};

module.exports = createRouter;